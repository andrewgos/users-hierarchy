
## User Hierarchy

Please contact andrewgosali@gmail.com if you have any questions or setup issues

### Requirements

- Please make sure that you have [NPM and Node.js](https://nodejs.org/en/) installed in your machine  


### Setup

- Clone this repo into your local machine
```shell
git clone git@bitbucket.org:andrewgos/users-hierarchy.git
```

- Install packages

```shell
npm install
```

### Testing

- main.js has been created to simulate the provided sample input and output

```shell
node main.js
```

- Run test suite

```shell
npm test
```