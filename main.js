/**
 * Most functions requested is developed as a method of object class (UserHierarchy)
 * for better data (users & roles) management and method calling (esp. test suite)
 *
 * This file is used mainly to run and simulate provided sample input and output
 */

const UserHierarchy = require("./src/UserHierarchy.js");

let presetRoles = [
  { Id: 1, Name: "System Administrator", Parent: 0 },
  { Id: 2, Name: "Location Manager", Parent: 1 },
  { Id: 3, Name: "Supervisor", Parent: 2 },
  { Id: 4, Name: "Employee", Parent: 3 },
  { Id: 5, Name: "Trainer", Parent: 3 }
];
let presetUsers = [
  { Id: 1, Name: "Adam Admin", Role: 1 },
  { Id: 2, Name: "Emily Employee", Role: 4 },
  { Id: 3, Name: "Sam Supervisor", Role: 3 },
  { Id: 4, Name: "Mary Manager", Role: 2 },
  { Id: 5, Name: "Steve Trainer", Role: 5 }
];

let hierarchy = new UserHierarchy();

hierarchy.setRoles(presetRoles);
hierarchy.setUsers(presetUsers);

console.log("getSubOrdinates(3)");
console.log(hierarchy.getSubOrdinates(3));

console.log("getSubOrdinates(1)");
console.log(hierarchy.getSubOrdinates(1));
