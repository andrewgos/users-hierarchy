const assert = require("assert");
const chai = require("chai");
const expect = chai.expect;

const UserHierarchy = require("../src/UserHierarchy.js");
const Role = require("./../src/Role.js");
const User = require("./../src/User.js");

let presetRoles = [
  { Id: 1, Name: "System Administrator", Parent: 0 },
  { Id: 2, Name: "Location Manager", Parent: 1 },
  { Id: 3, Name: "Supervisor", Parent: 2 },
  { Id: 4, Name: "Employee", Parent: 3 },
  { Id: 5, Name: "Trainer", Parent: 3 }
];
let presetUsers = [
  { Id: 1, Name: "Adam Admin", Role: 1 },
  { Id: 2, Name: "Emily Employee", Role: 4 },
  { Id: 3, Name: "Sam Supervisor", Role: 3 },
  { Id: 4, Name: "Mary Manager", Role: 2 },
  { Id: 5, Name: "Steve Trainer", Role: 5 }
];

describe("1. Classes Creation Testing", function() {
  describe("a. Role ", function() {
    it("should return an object with all keys when all required attributes is present", function() {
      let role = new Role({ Id: 1, Name: "System Administrator", Parent: 0 });
      expect(role).to.be.an("object");
      expect(role).to.have.all.keys("Id", "Name", "Parent");
    });
    it("should return {} when not all required attributes is present", function() {
      let role = new Role({ Id: 1, Name: "System Administrator" });
      expect(role).to.be.an("object");
      expect(role).to.be.empty;
    });
  });
  describe("b. User ", function() {
    it("should return an object with all keys when all required attributes is present", function() {
      let user = new User({ Id: 1, Name: "Adam Admin", Role: 1 });
      expect(user).to.be.an("object");
      expect(user).to.have.all.keys("Id", "Name", "Role");
    });
    it("should return {} when not all required attributes is present", function() {
      let user = new User({ Id: 1, Name: "Adam Admin" });
      expect(user).to.be.an("object");
      expect(user).to.be.empty;
    });
  });
});

describe("2. Function Testing", function() {
  let hierarchy = {};

  beforeEach(function() {
    hierarchy = new UserHierarchy();
  });

  describe("a. setRoles", function() {
    it("should successfully add roles when valid preset data is inserted", function() {
      expect(hierarchy.getRoles()).to.be.an("array").that.is.empty;
      hierarchy.setRoles(presetRoles);
      expect(hierarchy.getRoles()).to.be.an("array").that.is.not.empty;
    });
  });

  describe("b. setUsers", function() {
    it("should successfully add users when valid preset data is inserted", function() {
      expect(hierarchy.getUsers()).to.be.an("array").that.is.empty;
      hierarchy.setUsers(presetUsers);
      expect(hierarchy.getUsers()).to.be.an("array").that.is.not.empty;
    });
  });

  describe("c. getSubOrdinates", function() {
    beforeEach(function() {
      hierarchy.setRoles(presetRoles);
      hierarchy.setUsers(presetUsers);
    });
    it("should return array when valid value is present", function() {
      expect(hierarchy.getSubOrdinates(1)).to.be.an("array").that.is.not.empty;
    });
    it("should return [] when a non-integer is inserted", function() {
      expect(hierarchy.getSubOrdinates("abc")).to.be.an("array").that.is.empty;
      expect(hierarchy.getSubOrdinates({})).to.be.an("array").that.is.empty;
    });
  });

  describe("c. findChildRoles", function() {
    it("should return valid array when any char is inserted", function() {
      expect(hierarchy.findChildRoles(3)).to.be.an("array").that.is.empty;
    });
  });
});
