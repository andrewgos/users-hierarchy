/**
 * Most functions requested is developed here as an object class
 * for better data (users & roles) management and method calling (esp. test suite)
 */

const Role = require("./Role.js");
const User = require("./User.js");

class UserHierarchy {
  constructor() {
    this.roles = [];
    this.users = [];
  }

  /**
   * @return array
   */
  getRoles() {
    return this.roles;
  }

  /**
   * @return array
   */
  getUsers() {
    return this.users;
  }

  /**
   *
   * @param {*} presetRoles
   * @return void
   */
  setRoles(presetRoles) {
    presetRoles.forEach(role => {
      this.roles.push(new Role(role));
    });
  }

  /**
   *
   * @param {*} presetUsers
   * @return void
   */
  setUsers(presetUsers) {
    presetUsers.forEach(user => {
      this.users.push(new User(user));
    });
  }

  /**
   *
   * @param {*} userId
   * @return array
   */
  getSubOrdinates(userId) {
    if (isNaN(userId)) {
      return [];
    }

    // get the role of the user
    let roleId = this.users.find(user => user.getId() === userId).getRole();

    // if top level role, return users on any other role
    if (roleId === 0) {
      return this.users.filter(user => user.getRole() != 0);
    }

    // get the role object
    let role = this.roles.find(role => role.getId() === roleId);

    // get all child roles
    let childRoles = this.findChildRoles(role);

    // get all users based on the child roles
    return this.users.filter(user =>
      childRoles.some(role => role.getId() === user.getRole())
    );
  }

  /**
   *
   * @param {*} currentRole
   * @return array
   */
  findChildRoles(currentRole) {
    // get all child roles of the current recursion
    let childRoles = this.roles.filter(
      role => role.getParent() === currentRole.getId()
    );

    // exit statement of the recursion
    if (childRoles.length === 0) {
      return [];
    }

    let newChildRoles = childRoles;

    // recursively look for the child of this current recursion
    childRoles.forEach(childRole => {
      newChildRoles = [...newChildRoles, ...this.findChildRoles(childRole)];
    });

    return newChildRoles;
  }
}

module.exports = UserHierarchy;
