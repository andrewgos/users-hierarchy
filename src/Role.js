/**
 * Model class for Role
 * - stored in src folder for simplicity
 */

class Role {
  constructor(role) {
    if (
      !role.hasOwnProperty("Id") ||
      !role.hasOwnProperty("Name") ||
      !role.hasOwnProperty("Parent")
    ) {
      return undefined;
    }

    this.Id = role.Id;
    this.Name = role.Name;
    this.Parent = role.Parent;
  }

  /**
   * @return int
   */
  getId() {
    return this.Id;
  }

  /**
   * @return int
   */
  getParent() {
    return this.Parent;
  }
}

module.exports = Role;
