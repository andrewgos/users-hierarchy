/**
 * Model class for User
 * - stored in src folder for simplicity
 */

class User {
  constructor(user) {
    if (
      !user.hasOwnProperty("Id") ||
      !user.hasOwnProperty("Name") ||
      !user.hasOwnProperty("Role")
    ) {
      return undefined;
    }

    this.Id = user.Id;
    this.Name = user.Name;
    this.Role = user.Role;
  }
  /**
   * @return int
   */
  getId() {
    return this.Id;
  }

  /**
   * @return int
   */
  getRole() {
    return this.Role;
  }
}

module.exports = User;
